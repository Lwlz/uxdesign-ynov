import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import Layout from './hoc/Layout/Layout';
import Home from './containers/Home/Home';
// import './App.css';

class App extends Component {
  state = {
    goingauth: false
  };

  componentDidMount() {
    // this.props.onTryAutoSignup();
  }

  render() {
    let routes = (
      <Switch>
        {/* <Route path='/pricing' component={Contact} /> */}
        {/* <Route path='/signup' component={SignUp} /> */}
        {/* <Route path='/auth' component={asyncAuth} /> */}
        <Route path='/' exact component={Home} />
        {/* {this.props.isAuthenticated ? (
          <Route path='/dashboard' component={Dashboard} />
        ) : null}
        <Redirect to='/' /> */}
      </Switch>
    );

    return (
      <div>
        <Layout>{routes}</Layout>
      </div>
    );
  }
}

export default App;
