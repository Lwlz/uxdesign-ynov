import React, { Component } from 'react';
import classes from './Footer.css';

export default class Footer extends Component {
  render() {
    return (
      <footer className={classes.Footer}>
        <p
          style={{
            margin: 'auto'
          }}
        >
          Copyright © Socializer 2019
        </p>
        <ul>
          <li>one</li>
          <li>jjj</li>
          <li>kkokoe</li>
        </ul>
      </footer>
    );
  }
}
