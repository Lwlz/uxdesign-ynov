import React from 'react';
import ReactDOM from 'react-dom';
import classes from './NavigationItems.css';
import NavigationItem from './NavigationItem/NavigationItem';

const myRef = React.createRef();
// const ref = React.createRef();

const scrollToMyRef = () => {
  console.log('hello');
  // const tesNode = ReactDOM.findDOMNode(this.refs.test);
  // window.scrollTo(0, tesNode.current.offsetTop);
  // window.scrollTo(0, ref.current.offsetTop);
};

const navigationItems = props => (
  <ul className={classes.NavigationItems}>
    <NavigationItem link='/' exact>
      Why Socialize
    </NavigationItem>
    <NavigationItem clicked={scrollToMyRef} link='/' exact>
      Features
    </NavigationItem>
    <NavigationItem link='/pricing' exact>
      Pricing
    </NavigationItem>

    {props.isAuthenticated ? (
      <NavigationItem link='/dashboard/monit'>Dashboard</NavigationItem>
    ) : null}
    {!props.isAuthenticated ? (
      <NavigationItem
        className='btn btn-rounded btn-transparent btn-border--blue'
        link='/auth'
      >
        Log in
      </NavigationItem>
    ) : (
      <NavigationItem
        className='btn btn-rounded btn-transparent btn-border--blue'
        link='/logout'
      >
        Log out
      </NavigationItem>
    )}

    {!props.isAuthenticated ? (
      <NavigationItem
        className='btn btn-rounded btn-transparent btn-border--blue'
        link='/signup'
      >
        Sign Up
      </NavigationItem>
    ) : null}
  </ul>
);

export default navigationItems;
