import React from 'react';

import appLogo from '../../assets/img/logo_socializer.svg';
import classes from './Logo.css';

const logo = props => (
  <div className={classes.Logo} style={{ height: props.height }}>
    <img src={appLogo} alt='Socializer' />
  </div>
);

export default logo;
